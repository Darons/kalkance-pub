# Kalkance

Web application for managing SSL certificates.

**NOTE: Original source code is private, this repository is only intented for demonstration purposes.**

## Technical information

* **Framework**: [Angular](https://angular.io/) v6.1.0
* **UI**: [NG Bootstrap](https://ng-bootstrap.github.io/)
* **Libraries**: 
  * [NGX-Datatable](https://github.com/swimlane/ngx-datatable)
  * [Momentjs](https://momentjs.com/)
  * [RxJS](https://rxjs-dev.firebaseapp.com/)

## About my contribution

I developed the following modules:

* Login
* Users CRUD
* Roles CRUD
* Subroles CRUD
* Certificates Advanced Search

### Advance Search: 
Consisted on search certificates with the option to apply filters by date, region, URL, company name and/or certificate emitter.


In this repository you can find login and advance search modules.

![Login Interface](img/2.png?raw=true)  
![Login Interface](img/1.png?raw=true)  
![Login Interface](img/3.png?raw=true)  
![Login Interface](img/4.png?raw=true)