/**
 * Imports
 */

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup
  loading = false
  returnUrl: string
  logfail: boolean = false

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private ls: LoginService,
    private hs: HomeService
  ) {}

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: [
        '',
        [
          Validators.pattern(/^[-\w\.\$@\*\!]{1,30}$/),
          Validators.required,
          Validators.maxLength(30),
        ],
      ],
      password: ['', [Validators.required, Validators.maxLength(30)]],
    })
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
  }

  login() {
    const val = this.loginForm.value
    this.logfail = false

    if (this.loginForm.invalid) {
    } else {
      this.ls.login(new User(val.username, val.password)).subscribe(
        (res) => {
          localStorage.setItem('token_id', res.token)
          localStorage.setItem('user', val.username)
          this.router.navigate([this.returnUrl])
        },
        (err) => {
          if (err.status === 404) {
            this.logfail = true
          } else {
            this.hs.showSnackNotification(
              `Hubo un error al procesar la solicitud, código: ${err.status}`
            )
          }
        }
      )
    }
  }

  onChange() {
    this.logfail = false
  }
}
