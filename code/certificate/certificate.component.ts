/**
 * Imports
 */

@Component({
  selector: 'app-certificate',
  templateUrl: './certificate.component.html',
  styleUrls: ['./certificate.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CertificateComponent implements OnInit {
  constructor(
    public dialog: MatDialog,
    private cert: ListaCertificatesService,
    private toast: HomeService
  ) {}

  //Datatable properties.
  summaryPosition = 'bottom'
  loadingIndicator: boolean = false
  rows: Array<Certificates> = []
  message = {
    // Message to show when array is presented
    // but contains no values
    emptyMessage:
      'No hay información que cumpla con los criterios de búsqueda.',
    // Footer total message
    totalMessage: 'total',
  }
  scrollBarHorizontal = window.innerWidth < 1200

  //Models.
  filters = new Certificates()
  query: string

  ngOnInit() {
    this.getAll()
  }

  /**
   **Get all the certificates.
   */
  getAll() {
    this.loadingIndicator = true
    this.cert.allCerts().subscribe(
      (res) => {
        this.rows = res.certificates
        this.loadingIndicator = false
      },
      (error) => {
        this.toast.showSnackNotification('Error: ' + error)
      }
    )
  }

  /*
   **Search certifacates.
   */
  search() {
    this.cert.certs(this.query).subscribe(
      (res) => {
        this.rows = res.certificates
      },
      (error) => {
        this.toast.showSnackNotification('Error: ' + error)
      }
    )
  }

  /*
   **Open the filters dialog and set the filters when closed.
   */
  openFilters() {
    const ref = this.dialog.open(FilterDialog, {
      data: {
        fil: this.filters,
      },
    })

    ref.afterClosed().subscribe((result) => {
      if (result != undefined) {
        let a = result.fil
        for (let key in a) {
          if (key != 'idClient' && a[key] == '') {
            a[key] = 'null'
          }
        }
        if (result.date1) {
          this.filters.validFrom = moment(new Date(result.date1)).format(
            'YYYY-MM-DD'
          )
        }
        if (result.date2) {
          this.filters.validTo = moment(new Date(result.date2)).format(
            'YYYY-MM-DD'
          )
        }
        this.cert.findCerts(this.filters).subscribe(
          (res) => {
            if (res != null && res != undefined)
              this.rows = res.certifiedDtoOutList
            else this.rows = []
          },
          (error) => {
            this.toast.showSnackNotification('Error: ' + error)
          }
        )
        this.filters = result.fil
        this.query = ''
      }
    })
  }

  openDetail(row: any): void {
    this.dialog.open(DetailDialog, {
      data: {
        info: row,
      },
    })
  }
}
